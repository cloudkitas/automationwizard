package testcases;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.*;

import base.TestBase;
import pages.MainPage;

public class TestZero extends TestBase {
	
	
	public TestZero() {
		super();
	}
	
	MainPage mainPage; 
	
	@BeforeClass
	public void setUp() {
		Initialisation("headless");
		mainPage = new MainPage();
		
	}
	
	@Test(priority=1)
	public void getAllLinksNumber() {
		List<WebElement> allLinks = mainPage.getAllLinks();
		int numOfLinks = allLinks.size();
		System.out.println(numOfLinks);
	}
	
	@Test(priority=2)
	public void firstTest() {
		mainPage.goToPage();
		System.out.println("Hello");
	}
	
	@AfterClass
	public void tearDown() {
		driver.close();
	}
}
