package base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestBase {
	
	
	public static WebDriver driver;
	public static Properties prop;
	
	public TestBase() {
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream("src/config.properties");
			prop.load(ip);
			///Users/brunosantos/eclipse-workspace/saphana.qa.automation/src/main/java/util/config.properties	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static void Initialisation(String mode) {
		String browserName = prop.getProperty("browser");
		
		if (browserName.equals("chrome")) {
			if (browserName.equals("chrome")) {
				System.setProperty("webdriver.chrome.driver", 
						//"/src/drivers/chromedriver");
						"/Users/brunosantos/Documents/Test Automation Tools/Drivers/chromedriver");
				
				
				if (mode == "headless") {
					WebDriverManager.chromedriver().setup();
					ChromeOptions opt = new ChromeOptions();
					opt.setHeadless(true);
					driver = new ChromeDriver(opt);
				} else if (mode == "normal") {
					driver = new ChromeDriver();
				} else {
					System.out.println("driver mode not set :- DEFAULT NORMAL MODE");
					driver = new ChromeDriver();
				}
				
			}
			
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			driver.get(prop.getProperty("url"));
		}
	}
}
	
	

