package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import base.TestBase;

public class MainPage extends TestBase {
	
	

	@FindBy(linkText = "Dropdown")
	WebElement goToDropdownPage;
	
	
	public MainPage() {
		PageFactory.initElements(driver, this);
	}
	
	public void goToPage() {
		goToDropdownPage.click();
	}
	
	public List<WebElement> getAllLinks() {
		
		List<WebElement> allLinks = driver.findElements(By.tagName("a"));
		return allLinks;
		
	}
	

}
